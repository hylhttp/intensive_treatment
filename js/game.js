//创建一个游戏,设计游戏区域大小以及渲染环境
var game  = new Phaser.Game(1000,400,Phaser.AUTO,'#game');//'div的id'
/* game.scale.pageAlignHorizontally = true;
game.scale.pageAlignVertically = true; */
//定义游戏状态
var gameState = {};
//定义游戏主状态
 gameState.main = function(){};
var score = 0;//分数
var sum = 0;
var tiao = 0;
var timmer = null;
var GameOver = {
 
	preload: function () {
		//加载游戏结束图像
		game.load.image('gameover', 'img/1.jpg');
	},
	create: function () {
		//添加文字，显示最终游戏结果
		game.add.text(235, 310, "最终得分", { font: "bold 16px sans-serif", fill: "#46c0f9", align: "center" });
		game.add.text(350, 308, score.toString(), { font: "bold 20px sans-serif", fill: "#fff", align: "center" });
	},
};

gameState.main.prototype = {
	preload:function(){
		//预加载图片
		this.game.load.image('bg','img/bkg.png');
		this.game.load.image('dog','img/dog.png');
		this.game.load.image('dogleft','img/dogleft.png');
		this.game.load.image('line','img/line.png');
		this.game.load.image('master','img/money1.png');
		this.game.load.image('master_1','img/money2.png');
		this.game.load.image('master_2','img/money3.png');
		this.game.load.image('wood','img/wood.png');
		this.game.load.image('left','img/bkg-left.png');
		this.game.load.image('right','img/bkg-right.png');
		this.game.load.image('top','img/bkg-top.png');
		this.game.load.audio('jump','MP3/jumpp.mp3');
		this.game.load.audio('gamem','MP3/game.mp3');
		this.game.load.audio('dead','MP3/failed.wav');
	},
	create:function(){
		//添加背景图片
		this.game.add.sprite(0,0,'bg');
		this.dog = this.game.add.sprite(550,255,'dog');
		//怪兽（money）
		this.master0 = this.game.add.sprite(900,0,'master');
		this.master0.body.velocity.y = 100;
		this.text = game.add.text(0, 0, "Score: 0", { font: "bold 20px sans-serif", fill: "#46c0f9", align: "center" });
		var rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
		rightKey.onDown.add(this.right,this);
		var leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
		leftKey.onDown.add(this.left,this);
		var upKey = this.game.input.keyboard.addKey(Phaser.Keyboard.UP);
		upKey.onDown.add(this.up,this);
		/* this.game.time.events.overlap() */
		var downpKey = this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		downpKey.onDown.add(this.down,this);
		this.line = this.game.add.sprite(0,313.5,'line');
		this.left = this.game.add.sprite(0,0,'left');
		this.right = this.game.add.sprite(999,0,'right');
		this.top = this.game.add.sprite(0,0,'top');
		this.jump = this.game.add.audio('jump');
		this.gamem = this.game.add.audio('gamem');
		this.dead = this.game.add.audio('dead');
		this.gamem.play();
	},
	//默认为一秒刷新60次
	update:function(){
		this.game.physics.overlap(this.dog,this.line,this.land,null,this);
		this.game.physics.overlap(this.dog,this.left,this.over_left,null,this);
		this.game.physics.overlap(this.dog,this.right,this.over_right,null,this);
		this.game.physics.overlap(this.dog,this.top,this.over,null,this);
		this.game.physics.overlap(this.dog,this.master0,this.money0,null,this);
		this.game.physics.overlap(this.master0,this.line,this.xiaoshi0,null,this);
		this.game.physics.overlap(this.dog,this.master1,this.money1,null,this);
		this.game.physics.overlap(this.master1,this.line,this.xiaoshi1,null,this);
		this.game.physics.overlap(this.dog,this.master2,this.money2,null,this);
		this.game.physics.overlap(this.master2,this.line,this.xiaoshi2,null,this);
		this.game.physics.overlap(this.dog,this.master3,this.money3,null,this);
		this.game.physics.overlap(this.master3,this.line,this.xiaoshi3,null,this);
		this.game.physics.overlap(this.dog,this.master4,this.money4,null,this);
		this.game.physics.overlap(this.master4,this.line,this.xiaoshi4,null,this);
		this.game.physics.overlap(this.dog,this.master5,this.money5,null,this);
		this.game.physics.overlap(this.master5,this.line,this.xiaoshi5,null,this);
		this.game.physics.overlap(this.dog,this.master6,this.money6,null,this);
		this.game.physics.overlap(this.master6,this.line,this.xiaoshi6,null,this);
		this.game.physics.overlap(this.dog,this.master7,this.money7,null,this);
		this.game.physics.overlap(this.master7,this.line,this.xiaoshi7,null,this);
		this.game.physics.overlap(this.dog,this.master8,this.money8,null,this);
		this.game.physics.overlap(this.master8,this.line,this.xiaoshi8,null,this);
		this.game.physics.overlap(this.dog,this.master9,this.money9,null,this);
		this.game.physics.overlap(this.master9,this.line,this.xiaoshi9,null,this);
		//2$
		this.game.physics.overlap(this.dog,this.master10,this.money10,null,this);
		this.game.physics.overlap(this.master10,this.line,this.xiaoshi10,null,this);
		this.game.physics.overlap(this.dog,this.master11,this.money11,null,this);
		this.game.physics.overlap(this.master11,this.line,this.xiaoshi11,null,this);
		this.game.physics.overlap(this.dog,this.master12,this.money12,null,this);
		this.game.physics.overlap(this.master12,this.line,this.xiaoshi12,null,this);
		this.game.physics.overlap(this.dog,this.master13,this.money13,null,this);
		this.game.physics.overlap(this.master13,this.line,this.xiaoshi13,null,this);
		//3$
		this.game.physics.overlap(this.dog,this.master20,this.money20,null,this);
		this.game.physics.overlap(this.master20,this.line,this.xiaoshi20,null,this);
	},
	over_right:function(){
		this.dog.body.velocity.x = 0;
		this.dog.x = 940;
		},
	over_left:function(){
		this.dog.body.velocity.x = 0;
		this.dog.x = 0;
	},
	xialuo:function(){
		this.master = this.game.add.sprite(900,0,'master');
		this.master.body.gravity.y = 100;
	},
	left:function(){
		this.dog.loadTexture('dogleft',0);
		this.dog.body.velocity.x = -250;
	},
	right:function(){
		this.dog.loadTexture('dog',0);
		this.dog.body.velocity.x = 250;
	},
	up:function(){
		
		/* this.dog.loadTexture('dog',0); */
		if (tiao == 0){
			this.dog.body.velocity.y = -500;
			this.dog.body.gravity.y = 1000;
		}
		tiao ++;
	},
	down:function(){
		/* this.dog.loadTexture('dog',0); */
		this.dog.body.velocity.y = 100;
	},
	land:function(){
		this.dog.body.gravity.y = 0;
		this.dog.body.velocity.y = 0;
		this.dog.y = 255;
		tiao = 0;
	},
	updateText:function() {
	  this.text.setText("Score: " + score);
	},
	money0:function(){
		score++;this.jump.play();
		this.updateText();
		this.master0.loadTexture('wood1',0);//不存在，消失
		delete(this.master0);
		this.master1 = this.game.add.sprite(500,0,'master');
		this.master1.body.velocity.y = 110;
		sum += 1;
	},
	money1:function(){
		score ++;this.jump.play();
		this.updateText();
		this.master1.loadTexture('wood1',0);//不存在，消失
		delete(this.master1);
		
		this.master10 = this.game.add.sprite(700,0,'master_1');
		this.master10.body.velocity.y = 180;
		sum += 2;
	},
	money10:function(){
		score += 2;this.jump.play();this.updateText();
		this.master10.loadTexture('wood1',0);//不存在，消失
		delete(this.master10);
		this.master2 = this.game.add.sprite(300,0,'master');
		this.master2.body.velocity.y = 120;
		sum = 2;
	},
	money2:function(){
		score++;this.jump.play();this.updateText();
		this.master2.loadTexture('wood1',0);//不存在，消失
		delete(this.master2);
		this.master3 = this.game.add.sprite(700,0,'master');
		this.master3.body.velocity.y = 130;
		sum = 3;
	},
	money3:function(){
		score ++;this.jump.play();this.updateText();
		this.master3.loadTexture('wood1',0);//不存在，消失
		delete(this.master3);
		this.master11 = this.game.add.sprite(500,0,'master_1');
		this.master11.body.velocity.y = 190;
		sum = 4;
	},
	money11:function(){
		score +=2;this.jump.play();this.updateText();
		this.master11.loadTexture('wood1',0);//不存在，消失
		delete(this.master11);
		this.master4 = this.game.add.sprite(400,0,'master');
		this.master4.body.velocity.y = 140;
		sum = 4;
	},
	

	money4:function(){
		score++;this.jump.play();this.updateText();
		this.master4.loadTexture('wood1',0);//不存在，消失
		delete(this.master4);
		this.master20 = this.game.add.sprite(200,0,'master_2');
		this.master20.body.velocity.y = 220;
		sum = 5;
	},
	money20:function(){
		score+=3;this.jump.play();this.updateText();
		this.master20.loadTexture('wood1',0);//不存在，消失
		delete(this.master20);
		this.master5 = this.game.add.sprite(100,0,'master');
		this.master5.body.velocity.y = 150;
		sum = 5;
	},
	money5:function(){
		score++;this.jump.play();this.updateText();
		this.master5.loadTexture('wood1',0);//不存在，消失
		delete(this.master5);
		this.master12 = this.game.add.sprite(500,0,'master_1');
		this.master12.body.velocity.y = 160;
		sum = 6;
	},
	money12:function(){
		score+=2;this.jump.play();this.updateText();
		this.master12.loadTexture('wood1',0);//不存在，消失
		delete(this.master12);
		this.master6 = this.game.add.sprite(300,0,'master');
		this.master6.body.velocity.y = 160;
		sum = 6;
	},
	money6:function(){
		score++;this.jump.play();this.updateText();
		this.master6.loadTexture('wood1',0);//不存在，消失
		delete(this.master6);
		this.master7 = this.game.add.sprite(600,0,'master');
		this.master7.body.velocity.y = 170;
		sum = 7;
	},
	money7:function(){
		score++;this.jump.play();this.updateText();
		this.master7.loadTexture('wood1',0);//不存在，消失
		delete(this.master7);
		this.master13 = this.game.add.sprite(400,0,'master_1');
		this.master13.body.velocity.y = 200;
		sum = 8;
	},
	money13:function(){
		score+=2;this.jump.play();this.updateText();
		this.master13.loadTexture('wood1',0);//不存在，消失
		delete(this.master13);
		this.master8 = this.game.add.sprite(800,0,'master');
		this.master8.body.velocity.y = 180;
		sum = 8;
	},
	money8:function(){
		score++;this.jump.play();this.updateText();
		this.master8.loadTexture('wood1',0);//不存在，消失
		delete(this.master8);
		this.master9 = this.game.add.sprite(500,0,'master');
		this.master9.body.velocity.y = 190;
		sum = 9;
	},
	money9:function(){
		score++;this.jump.play();this.updateText();
		this.master9.loadTexture('wood1',0);//不存在，消失
		delete(this.master9);
		game.add.text(205, 210, "最终得分", { font: "bold 50px sans-serif", fill: "red", align: "center" });
		game.add.text(550, 208, score.toString(), { font: "bold 50px sans-serif", fill: "yellow", align: "center" });
		this.dead.play();
	},
	xiaoshi0:function(){
		this.master0.loadTexture('wood1',0);//不存在，消失
		delete(this.master0);
		
		this.master1 = this.game.add.sprite(500,0,'master');
		this.master1.body.velocity.y = 110;
		sum = 1;
	},
	
	xiaoshi1:function(){
		this.master1.loadTexture('wood1',0);//不存在，消失
		delete(this.master1);
		this.master10 = this.game.add.sprite(700,0,'master_1');
		this.master10.body.velocity.y = 120;
		sum = 2;
	},
	xiaoshi10:function(){
		this.master10.loadTexture('wood1',0);//不存在，消失
		delete(this.master10);
		this.master2 = this.game.add.sprite(300,0,'master');
		this.master2.body.velocity.y = 180;
		sum = 2;
	},
	xiaoshi2:function(){
		this.master2.loadTexture('wood1',0);//不存在，消失
		delete(this.master2);
		this.master3 = this.game.add.sprite(700,0,'master');
		this.master3.body.velocity.y = 130;
		sum = 3;
	},
	
	xiaoshi3:function(){
		this.master3.loadTexture('wood1',0);//不存在，消失
		delete(this.master3);
		this.master11 = this.game.add.sprite(500,0,'master_1');
		this.master11.body.velocity.y = 140;
		sum = 4;
	},
	xiaoshi11:function(){
		this.master11.loadTexture('wood1',0);//不存在，消失
		delete(this.master11);
		this.master4 = this.game.add.sprite(400,0,'master');
		this.master4.body.velocity.y = 140;
		sum = 4;
	},
	
	xiaoshi4:function(){
		this.master4.loadTexture('wood1',0);//不存在，消失
		delete(this.master4);
		this.master20 = this.game.add.sprite(200,0,'master_2');
		this.master20.body.velocity.y = 150;
		sum = 5;
	},
	xiaoshi20:function(){
		this.master20.loadTexture('wood1',0);//不存在，消失
		delete(this.master20);
		this.master5 = this.game.add.sprite(100,0,'master');
		this.master5.body.velocity.y = 150;
		sum = 5;
	},
	xiaoshi5:function(){
		this.master5.loadTexture('wood1',0);//不存在，消失
		delete(this.master5);
		this.master12 = this.game.add.sprite(500,0,'master_1');
		this.master12.body.velocity.y = 160;
		sum = 6;
	},
	xiaoshi12:function(){
		this.master12.loadTexture('wood1',0);//不存在，消失
		delete(this.master12);
		this.master6 = this.game.add.sprite(300,0,'master');
		this.master6.body.velocity.y = 160;
		sum = 6;
	},
	xiaoshi6:function(){
		this.master6.loadTexture('wood1',0);//不存在，消失
		delete(this.master6);
		this.master7 = this.game.add.sprite(600,0,'master');
		this.master7.body.velocity.y = 170;
		sum = 7;
	},
	
	xiaoshi7:function(){
		this.master7.loadTexture('wood1',0);//不存在，消失
		delete(this.master7);
		this.master13 = this.game.add.sprite(400,0,'master_1');
		this.master13.body.velocity.y = 180;
		sum = 8;
	},
	xiaoshi13:function(){
		this.master13.loadTexture('wood1',0);//不存在，消失
		delete(this.master13);
		this.master8 = this.game.add.sprite(800,0,'master');
		this.master8.body.velocity.y = 180;
		sum = 8;
	},
	xiaoshi8:function(){
		this.master8.loadTexture('wood1',0);//不存在，消失
		delete(this.master8);
		this.master9 = this.game.add.sprite(500,0,'master');
		this.master9.body.velocity.y = 190;
		sum = 9;
	},
	xiaoshi9:function(){
		this.master9.loadTexture('wood1',0);//不存在，消失
		delete(this.master9);
		game.add.text(205, 210, "最终得分", { font: "bold 50px sans-serif", fill: "red", align: "center" });
		game.add.text(550, 208, score.toString(), { font: "bold 50px sans-serif", fill: "yellow", align: "center" });
		this.dead.play();
	},
	wallCollision:function (head)
	{
		//检测狗头是否与界面边界发生碰撞
		if (head.x >= 1000 || head.x < 0 )
		{
			//游戏结束
			game.state.start('Game_Over');
		}
	},
};
//将我们自己定义的游戏状态添加到状态管理
game.state.add('mainState',gameState.main);
game.state.start('mainState');